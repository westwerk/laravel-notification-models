{{--
Make sure this file is not indented.
Indented html will be interpreted as a code block in markdown.
--}}
@component('mail::message')
{!! $body !!}
@endcomponent
