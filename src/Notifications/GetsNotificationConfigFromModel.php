<?php

namespace Westwerk\NotificationModels\Notifications;

use Westwerk\NotificationModels\Config;

trait GetsNotificationConfigFromModel
{

    public abstract function getNotificationConfigModel();

    /**
     * @return null|Config
     */
    public function getNotificationConfigFromModel()
    {
        $model = $this->getNotificationConfigModel();
        if (!$model) {
            return null;
        }
        $config = $model->notificationConfigs()->ofType(static::class)->first();
        return $config instanceof Config ? $config : null;
    }

    /**
     * @return null|Config
     */
    public function getNotificationConfig()
    {
        return $this->getNotificationConfigFromModel();
    }
}