<?php

namespace Westwerk\NotificationModels\Notifications;

use Illuminate\Notifications\Notification as BaseNotification;
use Westwerk\NotificationModels\Config;

class GeneralNotification extends BaseNotification
{

    use ConfiguresNotification;

    /**
     * @var array
     */
    private $via;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $actions;

    /**
     * @var bool
     */
    private $debug;


    public function __construct($via, Config $config, array $data = [], array $actions = [], bool $debug = false)
    {
        $this->via = (array)$via;
        $this->config = $config;
        $this->data = $data;
        $this->actions = $actions;
        $this->debug = $debug;
    }

    public static function make(Config $config)
    {
        $className = $config->type ?: static::class;
        return new $className($config);
    }

    public function via($notifiable): array
    {
        return $this->via;
    }

    public function getNotificationConfig(): Config
    {
        return $this->config;
    }

    public function getData($notifiable, string $channel): array
    {
        return $this->data;
    }

    public function getActions($notifiable, string $channel): array
    {
        return $this->actions;
    }

    public function isDebug(): bool
    {
        return $this->debug;
    }
}