<?php

namespace Westwerk\NotificationModels\Notifications;

use Illuminate\Mail\Markdown;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Twig\Environment;
use Twig\Error\Error as TwigError;
use Twig\Extension\SandboxExtension;
use Twig\Loader\ArrayLoader;
use Twig\Sandbox\SecurityPolicy;
use Twig\TwigFunction;
use Westwerk\NotificationModels\Config;
use Westwerk\NotificationModels\ConfigChannel;

/**
 * Trait ConfiguresNotification
 * @package Westwerk\NotificationModels\Notifications
 */
trait ConfiguresNotification
{

    /**
     * @return null|Config
     */
    public abstract function getNotificationConfig();

    public abstract function getData($notifiable, string $channel): array;

    public abstract function getActions($notifiable, string $channel): array;

    public abstract function isDebug(): bool;

    protected function getConfigChannel($channel): ConfigChannel
    {
        $config = $this->getNotificationConfig();
        $configChannel = $config ? $config->getChannel($channel) : null;
        return $configChannel instanceof ConfigChannel ? $configChannel : null;
    }

    /**
     * Returns a new twig environment. The templates given are identified in this environment by their array key and can
     * be rendered. The actions given are available from within the templates as functions named after the array key.
     * The actions are expected to be callable and render out the action when given a label.
     *
     * @param array|string[] $templates
     * @param array|callable[] $actions
     * @return Environment
     * @internal
     */
    private function prepareTwigEnvironment(array $templates, array $actions): Environment
    {
        // load templates from array, the array key is the name of the template
        $loader = new ArrayLoader($templates);

        // define twig environment
        $twig = new Environment($loader, [
            'cache' => false, // disables the cache, TODO: we might want to cache this, though
            'strict_variables' => false, // silently ignore invalid variables
            'autoescape' => false, // no need as we escape later with blade
            'debug' => $this->isDebug(), // whether to debug
        ]);

        // add function for each action
        foreach ($actions as $action => $actionRenderer) {
            $twig->addFunction(new TwigFunction($action, $actionRenderer));
        }

        // enable sandbox
        $twig->addExtension(new SandboxExtension(
            new SecurityPolicy( // allowed:
                ['filter', 'for', 'if', 'set', 'spaceless', 'verbatim', 'with'], // tags
                ['abs', 'batch', 'capitalize', 'date', 'date_modify', 'default', 'escape', 'first', 'format', 'join',
                    'json_encode', 'keys', 'last', 'length', 'lower', 'merge', 'number_format', 'replace', 'reverse',
                    'round', 'slice', 'sort', 'split', 'title', 'trim', 'upper'], // filters
                [/* none */], // object methods
                [/* none */], // object properties
                array_merge(
                    ['attribute', 'cycle', 'date', 'max', 'min', 'random', 'range'],
                    array_keys($actions) // our action functions
                ) // functions
            ),
            true // enable sandbox per default
        ));

        return $twig;
    }

    /**
     * Returns a closure that renders a template if given its name. The rendered content is then returned. In case of an
     * error the behaviour depends on whether debugging is enabled. If debugging is enabled any error that was caused by
     * the template is caught and the error message is returned. If debugging is disabled those errors are thrown.
     *
     * @param array $templates
     * @param array $actions
     * @param array $data
     * @return \Closure
     */
    private function prepareRender(array $templates, array $actions, array $data): \Closure
    {
        $twig = $this->prepareTwigEnvironment($templates, $actions);
        return function (string $name) use ($twig, $data) {
            try {
                return $twig->render($name, $data);
            } catch (TwigError $e) {
                if ($twig->isDebug()) {
                    return $e->getRawMessage();
                }
                throw $e;
            }
        };
    }

    public function toMail($notifiable): MailMessage
    {
        $channel = $this->getConfigChannel('mail');
        if (!$channel) {
            return null;
        }

        /** @var Markdown $markdown */
        $markdown = app(Markdown::class);

        /**
         * Originally the actions are just urls.
         * Map each action to its own closure which renders the action when given a label.
         * Each closure can render the action as html and text, depending on the value of $renderHtml.
         */
        $renderHtml = true;
        $actions = array_map(function (string $url) use ($markdown, &$renderHtml) : \Closure {
            return function (string $label = null) use ($markdown, &$renderHtml, $url) {
                // if no label is given, return whether this url is set
                if ($label === null) {
                    return !!$url;
                }
                // render if label given
                $view = 'notification-models::mail.action';
                $data = compact('label', 'url');
                if ($renderHtml) {
                    $html = $markdown->render($view, $data)->toHtml();
                    /**
                     * The html returned above is a full html document, thanks to the css inliner used in render().
                     * But what we actually want is the table from within the document, because we are embedding
                     * this html in a html mail which is already a html document itself.
                     */
                    if (!preg_match('/<table.+\/table>/s', $html, $match)) {
                        return '';
                    }
                    return $match[0];
                }
                return $markdown->renderText($view, $data)->toHtml();
            };
        }, $this->getActions($notifiable, 'mail'));

        // prepare rendering of templates
        $data = $this->getData($notifiable, 'mail');
        $render = $this->prepareRender([
            'subject' => $channel->payload('subject', ''),
            'body' => $channel->payload('body', ''),
        ], $actions, $data);

        // render body as html and text
        $view = 'notification-models::mail.message';
        $html = $markdown->render($view, ['body' => $render('body')]);
        $renderHtml = false;
        $text = $markdown->renderText($view, ['body' => $render('body')]);

        return (new MailMessage())
            ->subject($render('subject'))
            ->view(compact('html', 'text'));
    }

    public function toNexmo($notifiable): NexmoMessage
    {
        $channel = $this->getConfigChannel('nexmo');
        if (!$channel) {
            return null;
        }

        $data = $this->getData($notifiable, 'nexmo');
        $actions = []; // disable actions for nexmo, TODO: how can we show actions in a text message?
        $render = $this->prepareRender([
            'message' => $channel->payload('message', ''),
        ], $actions, $data);

        return new NexmoMessage($render('message'));
    }
}