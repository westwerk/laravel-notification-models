<?php

namespace Westwerk\NotificationModels;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $root = dirname(__DIR__);
        $this->loadMigrationsFrom($root . '/database/migrations');
        $this->loadViewsFrom($root . '/resources/views', 'notification-models');
    }
}