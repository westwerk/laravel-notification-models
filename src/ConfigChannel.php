<?php

namespace Westwerk\NotificationModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $id
 * @property string $channel
 * @property-read Config|null $config
 * @property array $payload
 * @method static $this ofChannel(string $channel)
 */
class ConfigChannel extends Model
{

    protected $table = 'notification_config_channels';

    protected $fillable = ['channel', 'config_id', 'payload'];

    protected $casts = [
        'payload' => 'array',
    ];


    public function config(): BelongsTo
    {
        return $this->belongsTo(Config::class, null, null, 'config');
    }

    public function scopeOfChannel(Builder $builder, string $channel)
    {
        $builder->where('notification_config_channels.channel', '=', $channel);
    }

    public function payload($key = null, $default = null)
    {
        return array_get($this->payload, $key, $default);
    }
}