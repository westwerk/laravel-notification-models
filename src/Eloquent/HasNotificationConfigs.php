<?php

namespace Westwerk\NotificationModels\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Westwerk\NotificationModels\Config;

trait HasNotificationConfigs
{

    public function notificationConfigs(): MorphMany
    {
        return $this instanceof Model ? $this->morphMany(Config::class, 'entity') : null;
    }
}