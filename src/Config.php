<?php

namespace Westwerk\NotificationModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Westwerk\NotificationModels\Notifications\GeneralNotification;

/**
 * @property-read int $id
 * @property string $type
 * @property string $entity_type
 * @property int $entity_id
 * @property array $payload
 * @property-read Collection|ConfigChannel[] $channels
 * @property-read Model|null $entity
 * @method static $this global()
 * @method static $this ofType(string $type)
 * @mixin Builder
 */
class Config extends Model
{

    protected $table = 'notification_configs';

    protected $fillable = ['type', 'entity_type', 'entity_id', 'payload'];

    protected $casts = [
        'payload' => 'array',
    ];


    public function channels(): HasMany
    {
        return $this->hasMany(ConfigChannel::class, 'config_id');
    }

    public function getChannel($channel = ''): ConfigChannel
    {
        return $this->channels->where('channel', '=', $channel)->first();
    }

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }

    public function scopeGlobal(Builder $builder)
    {
        $builder->whereNull('notification_configs.entity_type');
    }

    public function scopeOfType(Builder $builder, string $type)
    {
        $builder->where('notification_configs.type', '=', $type);
    }

    public function makeNotification()
    {
        return GeneralNotification::make($this);
    }
}