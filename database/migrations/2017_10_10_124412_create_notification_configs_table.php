<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationConfigsTable extends Migration
{

    public function up()
    {
        Schema::create('notification_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('entity_type')->nullable();
            $table->unsignedInteger('entity_id')->nullable();
            $table->longText('payload')->nullable();
            $table->timestamps();

            $table->index('type');
            $table->index('entity_type', 'entity_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('notification_configs');
    }
}
