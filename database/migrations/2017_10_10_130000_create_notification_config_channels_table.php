<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationConfigChannelsTable extends Migration
{

    public function up()
    {
        Schema::create('notification_config_channels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('config_id');
            $table->string('channel');
            $table->longText('payload')->nullable();
            $table->timestamps();

            $table->foreign('config_id')
                ->references('id')->on('notification_configs')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->index('channel');
        });
    }

    public function down()
    {
        Schema::dropIfExists('notification_config_channels');
    }
}
