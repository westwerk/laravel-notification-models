<?php

namespace Westwerk\NotificationModels\Test;

use Illuminate\Notifications\RoutesNotifications;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Testing\Fakes\MailFake;
use Mockery;
use Mockery\MockInterface;
use Orchestra\Testbench\TestCase as Orchestra;
use Westwerk\NotificationModels\Config;
use Westwerk\NotificationModels\ConfigChannel;
use Westwerk\NotificationModels\ServiceProvider;

class TestCase extends Orchestra
{

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    /**
     * @param array $channelPayload
     * @return Mockery\MockInterface|Config
     */
    protected function mockConfig(string $channel, array $channelPayload)
    {
        $config = Mockery::mock(Config::class);
        $config->shouldReceive('getChannel')
            ->withArgs([$channel])
            ->andReturn($this->createChannel($channel, $channelPayload));
        return $config;
    }

    protected function createChannel(string $channel, array $payload): ConfigChannel
    {
        return new ConfigChannel(compact('channel', 'payload'));
    }

    protected function mockUser()
    {
        /** @var MockInterface|RoutesNotifications $user */
        $user = Mockery::mock(RoutesNotifications::class);
        $user->email = 'test@example.com';
        return $user;
    }

    protected function assertMailContains($needles, $mail)
    {
        foreach (['text', 'html'] as $type) {
            $part = (string)$mail[$type];
            foreach ((array)$needles as $needle) {
                $this->assertContains($needle, $part);
            }
            $this->assertNotContains('<code>', $part, 'There was a code block in the html mail. Did you indent the message.blade.php file? You should not!');
        }
    }

    protected function assertMailNotContains($needles, $mail)
    {
        foreach (['text', 'html'] as $type) {
            $part = (string)$mail[$type];
            foreach ((array)$needles as $needle) {
                $this->assertNotContains($needle, $part);
            }
        }
    }

    protected function exampleData()
    {
        $config = $this->mockConfig('mail', [
            'subject' => 'This is the {{ subject }}!',
            'body' => <<<'EOL'
# Look at me, I'm a headline

This is a variable: {{ foobar }}.

This not: {{ nope }}.

{{ example("Visit example.com!") }}

{% verbatim %}
@if(true) Do not {{ $execute }} @blade code! @endif
{% endverbatim %}
EOL
        ]);
        $data = [
            'subject' => 'awesome subject',
            'foobar' => 'Hello, world!'
        ];
        $actions = [
            'example' => 'https://www.example.com/',
        ];
        $subject = 'This is the awesome subject!';
        $strings = [
            'This is a variable: Hello, world!.',
            'Look at me, I\'m a headline',
            'This not: .',
            'Visit example.com!',
            'https://www.example.com/',
            '@if(true) Do not {{ $execute }} @blade code! @endif',
        ];
        $not = [
            '{{ foobar }}',
            '{{ nope }}',
            '{{ action("example", "Visit example.com!") }}',
            '{% verbatim %}',
            '{% endverbatim %}',
            '&lt;', // this would indicate that html was escaped where it should not
        ];
        return [$config, $data, $actions, $subject, $strings, $not];
    }

    protected function assertOneMailWhichContains($subject, $needles, $not)
    {
        /** @var MockInterface|MailFake $mailFaker */
        $mailFaker = Mockery::mock(MailFake::class);
        $mailFaker->shouldReceive('send')
            ->withArgs(function ($view, $data) use ($not, $needles, $subject) {
                $this->assertEquals($subject, $data['subject']);
                $this->assertMailContains($needles, $view);
                $this->assertMailNotContains($not, $view);
                return true;
            })
            ->once();
        Mail::swap($mailFaker);
    }
}