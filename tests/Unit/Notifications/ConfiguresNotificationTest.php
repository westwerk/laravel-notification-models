<?php

namespace Westwerk\NotificationModels\Test\Unit\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Mockery;
use Westwerk\NotificationModels\Notifications\ConfiguresNotification;
use Westwerk\NotificationModels\Test\TestCase;

class ConfiguresNotificationTest extends TestCase
{

    public function testToMail()
    {
        $subject = 'This is the subject!';
        $body = 'Some funky body with {{ foobar }} and {{ example("Do it!") }}!';
        $mail = $this->mockTrait('mail', compact('body', 'subject'))->toMail(null);

        $this->assertInstanceOf(MailMessage::class, $mail);
        $this->assertArrayHasKey('html', $mail->view);
        $this->assertArrayHasKey('text', $mail->view);
        $this->assertEquals($subject, $mail->subject);
    }

    public function testToNexmo()
    {
        $nexmo = $this->mockTrait('nexmo', [
            'message' => 'Hi, there, please print "{{ foobar }}", but not "foobar"! Thank you!',
        ])->toNexmo(null);

        $this->assertEquals('Hi, there, please print "Hello, world!", but not "foobar"! Thank you!', $nexmo->content);
    }

    /**
     * @param array $channelPayload
     * @return Mockery\MockInterface|ConfiguresNotification
     */
    private function mockTrait(string $channel, array $channelPayload)
    {
        $trait = Mockery::mock(ConfiguresNotification::class);
        $trait->shouldReceive('getData')->andReturn(['foobar' => 'Hello, world!']);
        $trait->shouldReceive('getActions')->andReturn([
            'example' => 'https://www.example.com/',
        ]);
        $trait->shouldReceive('getNotificationConfig')->andReturn($this->mockConfig($channel, $channelPayload));
        $trait->shouldReceive('isDebug')->andReturn(false);
        return $trait;
    }
}