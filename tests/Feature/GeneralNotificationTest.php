<?php

namespace Westwerk\NotificationModels\Test\Feature;

use Westwerk\NotificationModels\Notifications\GeneralNotification;
use Westwerk\NotificationModels\Test\TestCase;

class GeneralNotificationTest extends TestCase
{

    public function testMail()
    {
        list($config, $data, $actions, $subject, $needles, $not) = $this->exampleData();
        $notification = new GeneralNotification('mail', $config, $data, $actions);
        $this->assertOneMailWhichContains($subject, $needles, $not);
        $this->mockUser()->notify($notification);
    }
}