<?php

namespace Westwerk\NotificationModels\Test\Feature;

use Illuminate\Notifications\Notification as BaseNotification;
use Mockery;
use Westwerk\NotificationModels\Notifications\ConfiguresNotification;
use Westwerk\NotificationModels\Test\TestCase;

class CustomNotificationTest extends TestCase
{

    public function testMail()
    {
        list($config, $data, $actions, $subject, $needles, $not) = $this->exampleData();

        $notification = Mockery::mock(
            BaseNotification::class, // base class
            ConfiguresNotification::class // trait
        );
        // mock methods required by the interface
        $notification->shouldReceive('getNotificationConfig')->andReturn($config);
        $notification->shouldReceive('getData')->andReturn($data);
        $notification->shouldReceive('getActions')->andReturn($actions);
        $notification->shouldReceive('isDebug')->andReturn(false);
        $notification->shouldReceive('via')->andReturn(['mail']);

        $this->assertOneMailWhichContains($subject, $needles, $not);
        $this->mockUser()->notify($notification);
    }
}